import configfile
import os
import mysql.connector as database
import csv

cnx = database.connect(
    user=configfile.config['DB_USER'],
    password=configfile.config['DB_PASSWORD'],
    host=configfile.config['DATABASE_HOST'],
    database=configfile.config['DATABASE'],
)


def main():
    csv_dir = configfile.config['CSV_DIR']
    cursor = cnx.cursor(dictionary=True,buffered=True)
    cursor.execute('SET NAMES utf8mb4;')
    cursor.execute('SET CHARACTER SET utf8mb4;')
    cursor.execute('SET character_set_connection=utf8mb4;')
    

    tables = [  'address', 
                'street', 
                'city', 
                'country',
                'venue', 
                'venue_address',
                'city_groupings',
                'city_groupings_has_translation_string',
                'city_groupings_has_city'] 
#    tables = ['address'] 
    [export(t, csv_dir, cursor) for t in tables]
    cursor.close()


def export(table_name, output_dir, cursor):
    sql  = 'SELECT * FROM ' + table_name
    cursor.execute(sql)
    results = cursor.fetchall()
    output_file = os.path.join(output_dir, table_name + '.csv')
    print (output_file)
    with open(output_file, 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile, lineterminator='\n', delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)
        for r in results:
            if 'address_geo' in r:
                if type(r['address_geo']) == bytearray:
                    r['address_geo'] = r['address_geo'].decode()
                    if r['address_geo'] == "bytearray(b'')":
                        r['address_geo'] = None
                else:
                    pass
            csv_writer.writerow(r.values())



    
if __name__ == "__main__":
    main()
