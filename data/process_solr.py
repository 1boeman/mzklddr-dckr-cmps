#!/usr/bin/env python
import configfile
import logging
from logging.handlers import RotatingFileHandler
import mysql.connector as database
import xml.etree.ElementTree as ET
import json
import requests
import argparse

cnx = database.connect(
    user=configfile.config['DB_USER'],
    password=configfile.config['DB_PASSWORD'],
    host=configfile.config['DATABASE_HOST'],
    database=configfile.config['DATABASE'],
)


logging.basicConfig(handlers=[RotatingFileHandler('/tmp/process_solr.log', maxBytes=100000, backupCount=5)],
            level=logging.WARN,
            format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
            datefmt='%Y-%m-%dT%H:%M:%S')


def config():
    delete_all()
    solr_schema()
    solr_config()


def main ():
    delete_all()
    events = get_events()
    cities = get_cities()
    index_solr_cities(cities)
    index_solr(events)
    commit()


def solr_schema():
    def solr_schema_request(command,data):
        solr = configfile.config['SOLR_API']
        solr_core = configfile.config['SOLRCORE']
        solr_schema = "/".join([solr, "cores", solr_core, 'schema'])
        call =  {command:data}
        r = requests.post(solr_schema,json=call )
        print(r)

    input_file = configfile.config['SOLR_FIELDS_FILE']
    print (input_file)
    with open(input_file) as f:
        root = ET.fromstring(f.read()) 
        for field in root:
            if field.tag == 'field':
                print(field.attrib['name'])
                solr_schema_request("delete-field",{"name" : field.attrib['name']})
                solr_schema_request("add-field",field.attrib)
            elif field.tag == 'copy-field':
                solr_schema_request("delete-copy-field",field.attrib)
                solr_schema_request("add-copy-field",field.attrib)
        print('Updated schema')


def solr_config():
    solr = configfile.config['SOLR_API']
    solr_core = configfile.config['SOLRCORE']
    solr_conf = "/".join([solr, "cores", solr_core, 'config'])
    input_file = configfile.config['SOLR_CONFIG_FILE']
    logging.info(input_file)
    with open(input_file) as j:
        data = json.load(j)
        for command in data:
            print(command)
            r = requests.post(solr_conf,json=command)
            print(r)
        print('Configured solr_config')


def index_solr_cities(cities):
    i = 0
    country_nl = {"1":"België" ,"2":"Duitsland","3":"Nederland"}
    city_groupings = get_city_groupings()
    cg = {}
    for row in city_groupings:
        row['translations'] = get_city_groupings(translations_for_id=row['group_id'])
        cg[row['group_id']] = row

    for c in cities:
        i+=1
        data = {}
        data['data_type_s'] = 'city'
        data['id'] = "city_id_" + str(c['city_id'])
        data['city_i'] = c['city_id']
        data['city_name_t'] = c['city_name']
        data['city_group_name_ss'] = [c['group_name']]
        if len (cg[c['group_id']]['translations']):
            for t in cg[c['group_id']]['translations']:
                data['city_group_name_ss'].append(t['string'])
        data['city_group_id_s'] = c['group_id']
        data['city_country_name_ss'] = [c['country_name']]
        data['city_country_name_ss'].append(country_nl[str(c['country_id'])])
        data['country_i'] = c['country_id']
        data['city_geo_p'] = c['city_geo'] 
        index_doc(data)
        print (str(i), 'solr city')


def index_solr(events):
    i = 0
    for e in events:
        i+=1
        #if i > 4:
         #   continue
        data = {} 
        data['data_type_s'] = 'event'
        data['id'] = e['event_id'] 
        # todo time_start and time_end
        if not e['date_start']:
            print ("error - no date start ")
            logging.error('error - no date start')
            logging.error(e)
            continue

        data['date_start_dt'] = e['date_start'].strftime('%Y-%m-%dT00:00:00Z')
        if e['date_end']:
            data['date_end'] = e['date_start'].strftime('%Y-%m-%dT00:00:00Z')
        data['event_title_t'] = e['event_title']
        data['event_description_t'] = e['event_description']
        data['event_description_txt_nl'] = e['event_description']
        data['event_description_txt_en'] = e['event_description']
        data['event_hash_s'] = e['event_hash']
 
        data['event_link_s'] = e['event_link']
        data['venue_id_s'] = e['venue_id']
        data['venue_title_s'] = e['venue_title']
        data['venue_description_t'] = e['venue_description']
        data['venue_link_s'] = e['venue_link']
        data['zip_s'] = e['zip']
        data['address_s'] = e['street_name'] 
        if not e['number']:
            logging.debug('no street number')
            logging.debug(e)
        else:
            data['address_s'] += " " + e['number']
        data['city_i'] = e['city_id']
        data['city_s'] = e['city_name'] 
        data['city_groupings_id_ss'] = []
        data['city_groupings_txt'] = []
        city_gr = get_city_groupings(city_id=e['city_id'])
        for cg in city_gr:
            data['city_groupings_id_ss'].append(cg['group_id'])
            data['city_groupings_txt'].append(cg['group_name'])
            if cg['translation']:
                data['city_groupings_txt'].append(cg['translation'])
        data['country_s'] = e['country_name']

        index_doc(data)
        print (str(i), 'solr event')


def get_city_groupings(city_id=None, translations_for_id=False):
    cursor = cnx.cursor(dictionary=True,buffered=True)
    if city_id:
        sql = '''select cg.*,ts.string as translation,ts.lang_code as translation_lang from city_groupings cg 
                    left join city_groupings_has_translation_string cghts on cghts.group_id = cg.group_id 
                    left join translation_string ts on ts.translation_id = cghts.translation_id 
                    inner join city_groupings_has_city cghc on cg.group_id = cghc.group_id and cghc.city_id = %s
        '''
        cursor.execute(sql,(city_id,))    
    elif translations_for_id:
        sql =""" select ts.* from city_groupings cg
                        inner join city_groupings_has_translation_string cghts on cghts.group_id = cg.group_id
                        inner join translation_string ts on ts.translation_id = cghts.translation_id
                        where cg.group_id = '""" + translations_for_id  + """'
        """
        cursor.execute(sql)
    else: 
        sql =""" select cg.* from city_groupings cg
                        order by cg.sort_order asc"""
        cursor.execute(sql)
    return cursor.fetchall()


def index_doc(data):
    solr = configfile.config['SOLRCORE_URL']
    solr = "/".join([solr,"update"])
    command = {"add":{"doc":data}}
    r = requests.post(solr,json=command )
    if r.status_code != 200:
        logging.error('index fail')
        logging.error(data)
        print (data)
    return r


def commit():
    solr = configfile.config['SOLRCORE_URL']
    solr_update = "/".join([solr,"update?commit=true"])
    r = requests.get(solr_update)
    print (r)


def get_cities():
    cursor = cnx.cursor(dictionary=True,buffered=True)
 
    sql = """
        select c.*,co.*,cg.group_name,cg.group_id from city c
        natural join country co
        left join city_groupings_has_city cghc 
            on cghc.city_id = c.city_id
        left join city_groupings cg 
            on cghc.group_id = cg.group_id
        order by co.country_id desc, cg.group_id, c.city_name
    """
    cursor.execute(sql)    
    return cursor.fetchall()


def get_events():
    cursor = cnx.cursor(dictionary=True,buffered=True)
    sql = '''
            select e.*, v.*, va.*, a.*, s.*, c.*, co.*
            from event e
            inner join venue_address  va
                on va.address_id = e.address_id
                and va.venue_id = e.venue_id
            inner join venue v on v.venue_id = e.venue_id
            inner join address a on a.address_id = e.address_id
            inner join street s on s.street_id = a.street_id
            inner join city c on c.city_id = s.city_id
            inner join country co on co.country_id = c.country_id
    '''
    cursor.execute(sql)    
    return cursor.fetchall()


def delete_all():
    solr = configfile.config['SOLRCORE_URL']
    solr_update = "/".join([solr,"update?commit=true"])
    r = requests.get(solr_update,json = {"delete": {"query": "*:*"}})
    if r.status_code != 200:
        print (data)
    return r


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', "--opts",)
    args = parser.parse_args()
    opts = args.opts
    if opts:
        if opts == 'config':
            config()
    else:
        main()
