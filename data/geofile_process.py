import configfile
import re
import json
import os
import argparse
import csv


def main():
    csv_dir = configfile.config['CSV_DIR']
    city_csv = os.path.join('./csv','city.csv')
    grouping_csv = os.path.join('./csv','city_groupings.csv')
    city_group_csv = os.path.join('./csv','city_groupings_has_city.csv')
 
    parser = argparse.ArgumentParser()
    parser.add_argument("geofile")
    args = parser.parse_args()

    with open(city_csv, mode='r') as file:
        csv_reader = csv.reader(file,delimiter =';')
        csv_list = {}
        for row in csv_reader:
            csv_list[row[0]] = row

    with open(grouping_csv, mode='r') as file:
        csv_reader = csv.reader(file,delimiter =';')
        csv_group_list = {}
        for row in csv_reader:
            csv_group_list[row[0]] = row

    with open(city_group_csv, mode='r') as file:
        csv_reader = csv.reader(file,delimiter =';')
        csv_city_group_list = {}
        for row in csv_reader:
            csv_city_group_list[row[1]] = row

    countries = ["",'BE','DE','NL']
    if os.path.isfile(args.geofile):
        with open (args.geofile) as f:
            data = json.load(f)
            for key in data:
                if "results" in data[key]:
                    r = data[key]["results"][0]
                elif "Id" in data[key]:
                    r = data[key]["Id"]["results"][0]
                else:
                    print (json.dumps(data[key],indent=2))
                    raise Exception("datastructure not recognized")

                # city
                if key in csv_list:
                    csv_list[key][-1]= ','.join([str(r["geometry"]["location"]["lat"]),\
                                                    str(r["geometry"]["location"]["lng"])])
                else:
                    for ac in r["address_components"]:
                        if ac['types'][0] == 'country':
                            country_id = countries.index(ac['short_name'])

                    geo = ','.join([str(r["geometry"]["location"]["lat"]),\
                                    str(r["geometry"]["location"]["lng"])])

                    city_name = r["address_components"][0]["long_name"]
                    if key == 'Array' and city_name == 'Noordwelle':
                        csv_list['1527932875'] = ['1527932875',city_name,country_id,geo]
                    else:
                        csv_list[key] = [key,city_name,country_id,geo]

                # grouping
                # check op aanwzigheid in koppeltabel:
                # dit veronderstelt een-op-veel relatie tussen stad en groep,
                # (dus niet veel-op-veel zoals koppeltabel suggereert)
                if key not in csv_city_group_list:
                    print(key,"not in koppeltable")
                    print(r)
                    country=''
                    grp = ''
                    group_id = ''
                    for ac in r["address_components"]:
                        if ac['types'][0] == 'country':
                            country = ac['long_name'].lower()
                        elif ac['types'][0] == 'administrative_area_level_1':
                            grp = ac['long_name'].lower().replace(" ","-")
                    if len(grp) and len(country):
                        group_id = grp + '_' + country 
                        print(group_id)
                    if group_id and group_id in csv_group_list:
                        print ('add',key,'to',group_id ) 
                        csv_city_group_list[key] = [group_id,key]
                    else:
                        print('group not found for', key)



    with open ('./csv/city.csv', mode='w',) as f2:
        fwriter = csv.writer(f2, delimiter=';',quotechar='"',quoting=csv.QUOTE_ALL)
        for x in csv_list:
            fwriter.writerow(csv_list[x]) 

    with open ('./csv/city_groupings_has_city2.csv', mode='w',) as f2:
        fwriter = csv.writer(f2, delimiter=';',quotechar='"',quoting=csv.QUOTE_ALL)
        for x in csv_city_group_list:
            fwriter.writerow(csv_city_group_list[x]) 


if __name__ == "__main__":
    main()
