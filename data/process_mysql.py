#!/usr/bin/env python
import configfile
import re
import logging
from logging.handlers import RotatingFileHandler
import time
import os
import glob
import mysql.connector as database
import hashlib
from lxml import etree
import csv
from datetime import date
import requests
import json
import traceback

cnx = database.connect(
    user=configfile.config['DB_USER'],
    password=configfile.config['DB_PASSWORD'],
    host=configfile.config['DATABASE_HOST'],
    database=configfile.config['DATABASE'],
)


logging.basicConfig(handlers=[RotatingFileHandler('/tmp/process_mysql.log', maxBytes=100000, backupCount=5)],
            level=logging.DEBUG,
            format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
            datefmt='%Y-%m-%dT%H:%M:%S')


def get_cursor():
    cursor = cnx.cursor(dictionary=True,buffered=True)
    cursor.execute('SET NAMES utf8mb4;')
    cursor.execute('SET CHARACTER SET utf8mb4;')
    cursor.execute('SET character_set_connection=utf8mb4;')
    return cursor


def main():
    output_dir = configfile.config['OUTPUTDIR']
    csv_dir = configfile.config['CSV_DIR']

    csv_map = {
        "country.csv":"country",
        "city.csv":"city",
        "street.csv":"street",
        "address.csv":"address",
        "venue.csv":"venue",
        "venue_address.csv":"venue_address",
        "city_groupings.csv":"city_groupings",
        "city_groupings_has_city.csv":"city_groupings_has_city",
        "translation_string.csv":"translation_string",
        "city_groupings_has_translation_string.csv":"city_groupings_has_translation_string",
    }

    for f in csv_map:
        _file = csv_dir + f
        process_csv(_file,csv_map[f])
    process_dir_contents(output_dir)
    get_geo()
    delete_empty_title_events()
    delete_old_events()
    print('[*] done')


def get_geo():
    process_geo_addresses_csv()
    #  get missing geocodes from api 
    get_geo_addresses()


def process_geo_addresses_csv():
    cursor = get_cursor()
    csv_dir = configfile.config['CSV_DIR']
    address_file = os.path.join(csv_dir,'address.csv')

    csv_data = csv.reader(open(address_file),delimiter=';')
    for row  in csv_data:
        print(row)
        geo = row[-1]
        if not geo or not len(geo):
            continue
        _id = row[0]
        query = ''' update address 
                    set address_geo = %s
                    where address_id = %s''' 
        cursor.execute (query, (geo,_id))
    cnx.commit() 
    cursor.close()


def get_geo_addresses():
    # geocode addresses
    api_key = configfile.config['MAPS_API_KEY']
    cache_dir = configfile.config['CACHE_DIR']  
    if not os.path.exists(cache_dir):
        os.makedirs(cache_dir)

    cursor = cnx.cursor(dictionary=True,buffered=True)
    query = ('''
     select * from address a 
        inner join street s on s.street_id = a.street_id
        inner join city c on c.city_id = s.city_id
        inner join country cn on c.country_id = cn.country_id
        where address_geo IS NULL ''')
    cursor.execute(query)
    r = cursor.fetchall()
    for row in r:
        try:
            print(row) 
            if row['street_name'] == 'onbekend':
                continue
            if not row['zip']:
                _zip = ''
            else :
                _zip = row['zip']

            address_string = " ".join([row['street_name'],row['number'] + ',',\
                                        _zip, row['city_name'],row['country_name']])
            print (address_string)
            p = {"address":address_string,"key":api_key}
            cache_file =  os.path.join(cache_dir,'address_' + str(row['address_id']))
            print (cache_file)
            if not os.path.isfile(cache_file):
                print ("requesting...")
                resp = requests.get('https://maps.googleapis.com/maps/api/geocode/json', params=p)
                if resp.ok:
                    with open(cache_file, "w") as f:
                        f.write(resp.text)
                        time.sleep(1)
                else:
                    logging.error('maps api failed') 
                    logging.error(row)
                    logging.error(resp)
            else:
                print ('found cached response', cache_file)
            with open(cache_file,"r") as f:
                data = json.load(f)
                if "results" in data:
                    r = data["results"][0]
                    geo = ','.join([str(r["geometry"]["location"]["lat"]),\
                                    str(r["geometry"]["location"]["lng"])])
                    print (geo)
                    query = ''' update address 
                                set address_geo = %s
                                where address_id = %s
                    ''' 
                    cursor.execute (query, (geo,row['address_id']))
        except Exception as e:
            logging.error(row) 
            logging.exception(e)
            
    cnx.commit() 
    cursor.close()


def process_csv(_file,_table):
    cursor = cnx.cursor(buffered=True)
    cursor.execute('SET NAMES utf8mb4;')
    cursor.execute('SET CHARACTER SET utf8mb4;')
    cursor.execute('SET character_set_connection=utf8mb4;')
    if os.path.isfile(_file):
        update_fields = {"venue":["venue_title","venue_description","venue_link"]}
        if _table in update_fields:
            csv_data = csv.reader(open(_file),delimiter=';')
            sql = """SELECT group_concat(COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS 
                      WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s"""
            cursor.execute(sql, (configfile.config['DATABASE'], _table))
            columns  = cursor.fetchone()
            column_list = columns[0].split(",")
            for _row  in csv_data:
                row = [v if v != "" else None for v in _row ]
                try:
                    sql = "insert into  " + _table + " ("+columns[0]+")" 
                    sql += " values("
                    sql += ",".join((' %s ', ) * len(row))
                    sql += ') '
                    sql += "on duplicate key"
                    sql += " update "
                    sql_list = []
                    for x in update_fields[_table] :
                        sql_list.append (x+' = values('+x+')') 
                    sql+= ",".join(sql_list)
                    print (sql)
                    print (row)
                    cursor.execute(sql, row)
                except Exception as e:
                    logging.error(row)
                    logging.exception(e)
                    continue

    cnx.commit() 
    cursor.close()


def process_dir_contents(_dir):
    print('process_dir_contents',_dir)
    glob_pattern = _dir + '/crawlers*.xml'
    harvest = glob.glob(glob_pattern)
    print (glob_pattern)
    print (harvest)
    for path in harvest:
        print(path)
        process_file(path,format='xml')


def process_file(path,format='xml'):
    print('process_file',path)
    if format == 'xml':
        data = process_xml_file(path)
        if data:
            save_data(data)
        else:
          print ('no data')


def process_xml_file(path):
    print ('process_xml_file',path)
    cursor = cnx.cursor(dictionary=True,buffered=True)
    cursor.execute('SET NAMES utf8mb4;')
    cursor.execute('SET CHARACTER SET utf8mb4;')
    cursor.execute('SET character_set_connection=utf8mb4;')

    location_id = path.split("/").pop().replace("crawlers.crawl_","").replace("crawlers.city_","").split(".")
    location_id.remove('xml')
    location_id = ".".join(location_id)
    location_id = re.sub('[^0-9a-zA-Z\.\-]+', '_', location_id)
    tree = etree.parse(path)
    locationData = {}
    eventData = []
    
    data = tree.xpath('//locationData')[0]
    locationData['venue_id'] = location_id
    locationData['venue_title'] = data.xpath('./title')[0].text
    vnue_link =  data.xpath('./link')[0].text
    if 'muziekladder.nl/locaties' in vnue_link:
        # replace old diverse locatielinks
        vnue_link = 'https://muziekladder.nl/locaties/'+location_id
    locationData['venue_link'] = vnue_link
    locationData['venue_description'] = data.xpath('./desc')[0].text
    # get_xml_txt(data,'./desc_en',target_dict = locationData, target_key = 'venue_description_en')
    locationData['city_id'] = data.xpath('cityno')[0].text

    sql = "select * from city where city_id = %s"
    cursor.execute(sql,(locationData['city_id'],))
    if cursor.rowcount == 0:
        '''city not yet present'''
        print(locationData['city_id'],': city_id not yet present error.')
        return None
    #locationData['street_name'] = data.xpath('street')[0].text
    get_xml_txt(data, 'street', target_dict = locationData, target_key = 'street_name', default_value='onbekend')
    get_xml_txt(data,'./streetnumber',target_dict = locationData, target_key = 'number')
    if locationData['number']:
        t2 = get_xml_txt(data,'./streetnumberAddition',return_it = True)
        if t2:
            locationData['number'] += t2
    get_xml_txt(data,'./zip',target_dict = locationData, target_key = 'zip')
    get_xml_txt(data,'./countryno',target_dict = locationData, target_key = 'country_id')

    events = tree.xpath('//event')
    for e in events:
        event = {}
        get_xml_txt(e,'./title',target_dict = event, target_key = 'event_title')
        get_xml_txt(e,'./date',target_dict = event, target_key = 'date_start')
        event['date_end'] = None
        get_xml_txt(e,'./link',target_dict = event, target_key = 'event_link')
        get_xml_txt(e,'./desc',target_dict = event, target_key = 'event_description')
        # get_xml_txt(e,'./desc_en',target_dict = event, target_key = 'event_description_en')
        get_xml_txt(e,'./img',target_dict = event, target_key = 'event_has_img')
        eventData.append(event.copy())
        
    cursor.close()
    return {"locationData":locationData, "eventData":eventData, "path":path}


def save_data(data):
    print ('save_data')
    address_id = save_venue(data)

    if address_id:
        data['address_id'] = address_id
        save_events(data)
    else:
        logging.debug('address_id not found ' + str(data))
        print ('address_id not found' + str(data) )


def delete_empty_title_events():
    cursor = cnx.cursor(buffered=True)
    logging.debug('delete whitespace titles')
    query = "delete from event where (event_title REGEXP '(^[[:space:]]+$)');"
    cursor.execute(query)
    cnx.commit() 
    cursor.close()


def delete_old_events():
    logging.debug('delete old media')
    old = '2' #days
    cursor = cnx.cursor(buffered=True)
    query = 'select event_id from muziekladder_db.event where date_start <= DATE_SUB(CURDATE(), INTERVAL '+ old +' DAY);'
    cursor.execute(query)
    rows = cursor.fetchall()
    for _id in rows: 
        try:
            query = 'select media_id from event_has_media where event_id = ' + str(_id[0])
            cursor.execute(query)
            media_id = cursor.fetchone()
            if media_id:
                query = 'delete from event_has_media where event_id = ' + str(_id[0])
                cursor.execute(query)
                try:
                    query = 'select local_path from media where media_id = ' + str(media_id[0])
                    query = 'delete from media where media_id = ' + str(media_id[0])
                    cursor.execute(query)
                    logging.debug("deleted old media:" + str(media_id[0]))
                    #logging.debug(cursor.fetchone())
                except Exception as e:
                    logging.exception(e)
     
            query = 'delete from post_has_event where event_id = ' + str(_id[0])
            cursor.execute(query)
            media_id = cursor.fetchone()
     
            query = 'delete from event where event_id = ' + str(_id[0])
            cursor.execute(query)
            logging.debug('deleted old event: ' + str(_id[0]))
        except Exception as e:
            logging.exception(e)

    cnx.commit() 
    cursor.close()


def save_events(data):
    print ('save events')
    cursor = cnx.cursor(dictionary=True,buffered=True)
    cursor.execute('SET NAMES utf8mb4;')
    cursor.execute('SET CHARACTER SET utf8mb4;')
    cursor.execute('SET character_set_connection=utf8mb4;')
    date_today = date.today().isoformat()
   
    _hash_keys =  ['event_title', 'date_start','event_description','event_link','venue_id','address_id'] # used to build hash identifier
    needed_keys = ['event_title', 'date_start', 'event_description','event_link','venue_id','address_id'] # minimum required keys
    insert_keys = ['event_title', 'date_start','time_start','date_end','time_end','event_description','event_link','venue_id','address_id','event_hash' ]
    for ed in data['eventData']:
      try:
          if ed['event_link'] and ed['event_link'][0:4].lower() != 'http':
            logging.debug('skipping - this link would 404' + ed['event_link'])
            logging.debug(str(ed))
            print('skipping this link would 404')
            print (str(ed))
            continue
          if ed['date_start'] < date_today:
            if 'date_end' not in ed or not ed['date_end'] or ed['date_end'] < date_today:
              print ('*'*30)
              print ('skipping event because it is in the olden times')
              print ('date_start', ed['date_start'])
              print (ed)
              print ('*'*30)
              continue
          
          ed['venue_id'] = data['locationData']['venue_id']
          ed['address_id'] = data['address_id']
          error = ''
          _hash_list = []
          for k in needed_keys:
              if k not in ed or not ed[k]:
                  error = k
          if len(error):
              print ( "missing " + error + " in " + data['path'] )
              continue

          for k in _hash_keys:
              _hash_list.append(str(ed[k]))
          _hash = hashlib.sha256("".join(_hash_list).encode("utf-8")).hexdigest()
          ed['event_hash'] = _hash
          query = ('select event_id from muziekladder_db.event where event_hash = %s and venue_id = %s')
          print ('execute hash query',_hash)
          cursor.execute(query, (_hash, data['locationData']['venue_id']))

          values = list( map(ed.get, insert_keys) )
          if cursor.rowcount == 0 :
              print
              keystring = ",".join(insert_keys)
              # insert event
              print('**insert event')
              sql = ' insert into muziekladder_db.event (' + keystring + ') values ( %s, %s, %s, %s, %s, %s, %s, %s, %s, %s ); '
              cursor.execute(sql,values )
              event_id = cursor.lastrowid
          else:
              # update event: 
              rows = cursor.fetchall()
              event_id = rows[0]['event_id']
              set_clause = [ ' `' + k + '` = %s' for k in insert_keys ] 
              sql = 'update muziekladder_db.event set '
              sql += (', '.join(set_clause) +  ' where event_id = ' + str(event_id) +'')
              print('update')
              cursor.execute(sql,values)

          # insert image related to event 
          if ed['event_has_img'] and len(ed['event_has_img']) < 2048:
            query = ('select media_id from media where url = %s')
            cursor.execute(query,(ed['event_has_img'],))
            if cursor.rowcount == 0:
              print (ed['event_has_img'])
              sql = 'insert into muziekladder_db.media ( url ) values ( %s )'
              cursor.execute(sql, (ed['event_has_img'],))
              media_id = cursor.lastrowid
            else :
              rows = cursor.fetchall()
              media_id = rows[0]['media_id']
            
            query = 'select * from muziekladder_db.event_has_media where event_id = %s and media_id = %s' 
            cursor.execute(query,(event_id,media_id))
            if cursor.rowcount == 0:
              sql = 'insert into muziekladder_db.event_has_media (event_id, media_id) value (%s,%s)'
              cursor.execute(sql, (event_id,media_id))
      except Exception as e:
          logging.error(row) 
          logging.exception(e)

    cnx.commit() 
    cursor.close()


def save_venue(data):
    print ('save venue',data) 
    cursor = cnx.cursor(dictionary=True,buffered=True)
    cursor.execute('SET NAMES utf8mb4;')
    cursor.execute('SET CHARACTER SET utf8mb4;')
    cursor.execute('SET character_set_connection=utf8mb4;')


    ld = data['locationData']
    if ld['zip']:
        ld['zip'] = ld['zip'][:10]
    venue_id = ld['venue_id']
    # venue
    query = ('select * from muziekladder_db.venue where venue_id  = %s')
    cursor.execute(query, (venue_id,))
    rows = cursor.fetchall()
    #    keys =  ( "venue_id", "venue_title", "venue_description", "venue_description_en","venue_link" )
    keys =  ( "venue_id", "venue_title", "venue_description", "venue_link" )
 
    values = list( map(ld.get, keys) )
    keystring = ",".join(keys)
    if cursor.rowcount == 0:
        # insert
        sql = 'INSERT INTO muziekladder_db.venue (' + keystring + ') VALUES (%s, %s , %s , %s );'
    else:
        # update
        sql = 'update muziekladder_db.venue set '
        set_clause = [ ' `' + k + '` = %s' for k in keys ] 
        sql += (', '.join(set_clause) +  ' where venue_id = "' + venue_id +'"')

    cursor.execute(sql,values)
    
    # street
    sql = "select street_id from street  where city_id = %s and LOWER(street_name) = LOWER(%s)"
    cursor.execute(sql,(ld['city_id'],ld['street_name'],))
    if cursor.rowcount == 0:
        sql = "insert into street (city_id, street_name) values (%s,%s)"
        cursor.execute(sql,(ld['city_id'],ld['street_name'],))
        street_id = cursor.lastrowid
    else:
        rows = cursor.fetchall()
        street_id = rows[0]['street_id']

    # address
    sql = "select address_id from address where street_id = %s and number = %s"
    if not ld['number']:
        ld['number'] = ""
    cursor.execute(sql, (street_id,ld['number']))
    if cursor.rowcount == 0:
        print('*******************insert')
        logging.debug('insert address ' + str([street_id,ld['zip'],ld['number']]))
        print(street_id, ld['zip'], ld['number'])
        sql = 'insert into address (street_id,zip,number,address_geo) values (%s,%s,%s,NULL)'
        cursor.execute(sql, (street_id,ld['zip'],ld['number']))
        address_id = cursor.lastrowid
    else:
        rows = cursor.fetchall()
        address_id = rows[0]['address_id']

    venue_id
    # venue_address
    sql = "select * from venue_address where address_id = %s and venue_id = %s"
    cursor.execute(sql,(address_id,venue_id))
    if cursor.rowcount == 0:
        sql = 'insert into venue_address (address_id,venue_id, venue_address_title) values (%s,%s,%s)'
        cursor.execute(sql,(address_id, venue_id, ld['venue_title']))
    
    cursor.close()

    return address_id


# use for optional data
def get_xml_txt(tree, xpath_query, target_dict={}, target_key='', return_it = False, default_value = None):
    tmp_list = tree.xpath(xpath_query)
    if len(tmp_list) and tmp_list[0].text and len(tmp_list[0].text):
        if return_it:
            return tmp_list[0].text
        else:
            target_dict[target_key] = str(tmp_list[0].text)
    elif return_it:
        return False
    elif default_value:
        target_dict[target_key] = default_value
    else:
        target_dict[target_key] = None
        


if __name__ == "__main__":
    main()
