import httpx
import time
import requests
import os
import shutil
import re
import json
from parsel import Selector
import config as conf


site_root = conf.SITE_ROOT
output_path = conf.OUTPUT_PATH
output_path_image = os.path.join(output_path,'images')
headers={'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0'}
timeout=120.0

def main():
    os.makedirs(output_path, exist_ok=True)
    pages = get_pages_urls()
    parse_pages(pages)


def get_pages_urls():
    yr = ['2023','2024'] 
    pages = []
    for y in yr:
        for i in range(1,13):
            yr_mnth = y + str(i).zfill(2)
            for j in range(12):
                page = yr_mnth + '?page=' + str(j)
                pages.append(site_root + '/nl/archief/'+page)
    return pages


def parse_pages(pages):
    for p in pages:
        r=requests.get(p, timeout=timeout, headers=headers)
        selector = Selector(text=r.text)
        articles = selector.xpath('//article')
        time.sleep(3)
        for a in articles:
            parse_node(a.css('.node-readmore a').attrib['href'])


def download_file(url, node_id):
    local_filename = url.split('/')[-1].split('?')[-2]
    local_filename = node_id + '_' + local_filename
    output_dir = output_path_image
    os.makedirs(output_dir, exist_ok=True)
    output_path = os.path.join(output_dir, local_filename)
    with requests.get(url, stream=True, timeout=timeout) as r:
        with open(output_path, 'wb') as f:
            shutil.copyfileobj(r.raw, f)
    return output_path


def parse_node(path):
    obj = {}
    file_name = re.sub('[^0-9a-zA-Z]+', '_', path) + '.json'
    file_path = os.path.join(output_path,file_name)
    if os.path.isfile(file_path):
        print('[*] skip bc already exists: ', path)
        return
    else:
        time.sleep(5)
    r = requests.get(site_root + path, timeout=timeout, headers=headers)
    sel = Selector(text=r.text)

    obj['title'] = sel.css('h1::text').get().strip()
    obj['node_id'] = sel.css('article').attrib['class'].split(" ")[0]
    obj['created'] = sel.css('article').attrib['data-created']
    print(obj['node_id'])
    img = sel.css('.field-item img')
    if img:
        obj['img'] = img.attrib['src']
        local_path = download_file(obj['img'], obj['node_id'])
        print(local_path)
        obj['img_local'] = local_path
    if sel.css('.city_in_node'):
        obj['cityAsText'] = sel.css('.city_in_node span::text').get().strip()
        obj['venueAsText'] = " ".join(sel.css('.venue_in_node ::text').getall()).strip() 
        obj['link'] = sel.css('.event_link_in_node a').attrib['href']
        obj['dates'] = parse_dates(sel.css('.dates_in_node li::text').getall())
        obj['type'] = sel.css('.soort_in_node ::text').get().strip() 
        obj['uid'] = sel.css('.user_link_in_node').attrib['data-uid']
        obj['description'] = " ".join(sel.css('.description_in_node::text').getall()).strip()
    else:
        try:
            obj['uid'] = sel.css('#page').attrib['data-node_uid']
        except Exception:
            print ('[*] skipping ', obj['node_id'])
            return False
        obj['type'] = 'artist'
        obj['description'] = sel.css(".field-name-body .field-item p").getall()
        obj['link'] = sel.css('.field-name-field-url ::text').get()
        obj['media_link'] =  sel.css('.field-name-field-media-url .field-item::text').get()


    with open(file_path, 'w') as fp:
        json.dump(obj, fp)
    print ('[*] write: ', file_path)


def parse_dates(_list):
    maanden = ["bla",'januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december']
    months = ["bla",'January','February','March','April','May','June','Juli','August','September','October','November','December']
 
    dates = []
    for d in _list:
        vals = re.split(r"[\s,]+", d)
        if vals[2] in maanden:
            dates.append( "-".join([vals[1], str(maanden.index(vals[2])), vals[3]]))
        else: 
            dates.append( "-".join([vals[1], str(months.index(vals[2])), vals[3]]))
 
    return dates


if __name__ == "__main__":
    main()
