#!/usr/bin/env python
import configfile
import logging
from logging.handlers import RotatingFileHandler
import time
import os
import glob
import mysql.connector as database
import hashlib
from lxml import etree
import csv
import re
from datetime import date
import requests
import json
import shutil
import datetime


cnx = database.connect(
    user=configfile.config['DB_USER'],
    password=configfile.config['DB_PASSWORD'],
    host=configfile.config['DATABASE_HOST'],
    database=configfile.config['DATABASE'],
)


logging.basicConfig(handlers=[RotatingFileHandler('/tmp/process_node_import.log', maxBytes=100000, backupCount=5)],
            level=logging.DEBUG,
            format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
            datefmt='%Y-%m-%dT%H:%M:%S')
logger = logging.getLogger(__name__)


node_image_dir = os.path.join('/app/upload_archive','original/old_site')


def get_cursor():
    cursor = cnx.cursor(dictionary=True,buffered=True)
    cursor.execute('SET NAMES utf8mb4;')
    cursor.execute('SET CHARACTER SET utf8mb4;')
    cursor.execute('SET character_set_connection=utf8mb4;')
    return cursor


def main():
    os.makedirs(node_image_dir, exist_ok=True)
    input_dir = './site_crawl/output'
    files = glob.glob(input_dir+ '/*.json')

    cursor = get_cursor()
    for _file in files:
        with open(_file) as f:
            d = json.load(f)
            print(_file)
            try:
                insert(d, cursor)
            except Exception as e:
                logger.exception(e)
                continue


    cnx.commit() 
    cursor.close()


def strip_tags(text):    
    return re.sub('<[^<]+?>', '', text)


def insert(data, cursor):
    # insert one record 
    print(data)
    media_id = None
    if 'img_local' in data:
        img = data['img_local']
        file_name = img.split("/")[-1]
        file_dest_path = os.path.join(node_image_dir,file_name)
        db_file_path = "/".join(file_dest_path.split('/')[-2:])
        file_origin_path = os.path.join('./site_crawl',img)
        shutil.copyfile(file_origin_path,file_dest_path) 
        sql = """ insert into media (local_path) values (%s);"""
        cursor.execute(sql,(db_file_path,))
        media_id = cursor.lastrowid 
    sql = """ insert into post 
            (post_id,user_id, post_title, post_data, post_type, published,
                last_update, date_created)
            values (%s,%s,%s,%s,%s,0,from_unixtime(%s),from_unixtime(%s))
        """
    _id = int(data['node_id'].split("-")[1])
    _type = 'event'

    if data['type'] == 'artist':
        _type = 'artist'
        post_data = json.dumps({
            "link":data['link'],
            "link2":data['media_link'],
            "descText": "\n".join(map(strip_tags, data['description'])),
            "title": data["title"],
            "user_id": data['uid'],
        })
        play_date = datetime.datetime.today()
    else:
        post_data = json.dumps({
            "checkboxCity": True,
            "checkboxVenue": True,
            "cityAsText": data["cityAsText"],
            "citySelect": None,
            "descText": data["description"],
            "venueAsText": data['venueAsText'],
            "venueSelect": None,
            "eventDate": None,
            "eventDates": ",".join(data['dates']),
            "eventType": data['type'],
            "link": data['link'],
            "media": media_id,
            "submitThis": "y",
            "title": data["title"],
            "user_id": data['uid'],
        })
        #mydate = data['dates'][0].split('-')
        #play_date = datetime.date(int(mydate[-1]), int(mydate[-2]), int(mydate[-3]))
    #last_day_previous_month = play_date - datetime.timedelta(days=120)
    #mysql_date = last_day_previous_month.strftime('%Y-%m-%d')
    _timestamp = int(data['created'])
    values = (_id, 
        data['uid'], 
        data['title'],
        post_data,
        _type,
        _timestamp,
        _timestamp,
    )

    try:
        cursor.execute(sql,values)
    except Exception as e:
        logger.exception(e)

    post_id = cursor.lastrowid
    if media_id:
        sql = """insert into post_has_media 
                (post_id,media_id) values (%s,%s)"""
        cursor.execute(sql,(post_id,media_id))


if __name__ == "__main__":
    main()
