#!/bin/bash

cd "$(dirname "$0")"

echo '[*] process mysql'
./docker_run_process_mysql.sh

echo '[*] process posts'
podman exec muziek_front flask cmd activate_post_events

echo '[*] csv backup'
./backup_docker_run_export_to_csv.sh

echo '[*] process solr'
./docker_run_process_solr.sh

echo '[*] process posts solr'
./docker_run_process_solr_posts.sh

echo '[*] clear front end cache'
podman exec muziek_front flask clear-cache

echo '[*] refresh csv'
./backup_docker_run_export_to_csv.sh
