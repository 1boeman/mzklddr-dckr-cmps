#!/bin/env bash
cd "$(dirname "$0")"
LOCATION="../mzklddr/instance/user_data/tmp/"

find $LOCATION -name '*_*' -type f -mmin +60 -delete
